<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>KLGameField</name>
    <message>
        <location filename="KLGameField.cpp" line="318"/>
        <source>Load colony from file</source>
        <translation>Kolonie von der Datei einladen</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="319"/>
        <location filename="KLGameField.cpp" line="394"/>
        <source>This application (*.kgol)</source>
        <translation>Diese App (*.kgol)</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="332"/>
        <location filename="KLGameField.cpp" line="408"/>
        <source>Open file failed</source>
        <translation>Dateiladen Versager</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="338"/>
        <location filename="KLGameField.cpp" line="353"/>
        <location filename="KLGameField.cpp" line="363"/>
        <location filename="KLGameField.cpp" line="375"/>
        <source>Invalid file format</source>
        <translation>Ungültiges Dateiformat</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="387"/>
        <location filename="KLGameField.cpp" line="427"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="393"/>
        <source>Save colony current state</source>
        <translation>Den Kolonie zustand sparen</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="434"/>
        <source>Choose cells color</source>
        <translation>Zellefarbe wahlen</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="447"/>
        <source>Choose background color</source>
        <translation>Hintergrundfarbe wahlen</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="194"/>
        <source>Game Of Life</source>
        <translation>Lebenspiel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="213"/>
        <source>Game</source>
        <translation>Spiel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="85"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="214"/>
        <source>Settings</source>
        <translation>Setzen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="89"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="215"/>
        <source>Colors...</source>
        <translation>Farben...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="216"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="195"/>
        <source>New Game</source>
        <translation>Neues Spiel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="196"/>
        <source>Next Step</source>
        <translation>Nächster Schritt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="169"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="197"/>
        <source>Start/Stop Evolution</source>
        <translation>Start/Stop die Evolution</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="199"/>
        <source>Start Evolution</source>
        <translation>Start die Evolution</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="177"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="201"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="202"/>
        <source>About</source>
        <translation>Über diese App</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="191"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="203"/>
        <source>Save...</source>
        <translation>Sparen...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="194"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="205"/>
        <source>Save colony current state</source>
        <translation>Den Koloniezustand sparen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="203"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="207"/>
        <source>Open...</source>
        <translation>Laden...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="209"/>
        <source>Load colony from file</source>
        <translation>Den Koloniezustand einladen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="211"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="211"/>
        <source>Cells</source>
        <translation>Zellen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="212"/>
        <source>Background</source>
        <translation>Hintergrund</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="29"/>
        <source>Generation Change Speed</source>
        <translation>Veränderengeschwindigkeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="39"/>
        <source>Set or erase a single cell by double click or drag a line with left button pressed</source>
        <translation>Mausdoppelklick um die Einzelle setzen oder löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="57"/>
        <source>A simple Game Of Life Qt realization</source>
        <translation>Die einfrache Lebenspielrealisation</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="85"/>
        <source>Start evolution</source>
        <translation>Start die Evolution</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="85"/>
        <source>Stop evolution</source>
        <translation>Stop die Evolution</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="89"/>
        <source>Generation: %1</source>
        <translation>Generation %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="94"/>
        <source>Colony is empty</source>
        <translation>Leerkolonie</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="31"/>
        <source>Simple Game Of Life realization</source>
        <oldsource>Simple Game Of Life realization for KDE</oldsource>
        <translation>Die einfache Lebenspielrealisation</translation>
    </message>
    <message>
        <location filename="main.cpp" line="32"/>
        <source>Created by: </source>
        <translation>Erstellt von: </translation>
    </message>
    <message>
        <location filename="main.cpp" line="36"/>
        <source>Usage %1 [options]</source>
        <translation>Benutzen: %1 [Argumente]</translation>
    </message>
    <message>
        <location filename="main.cpp" line="38"/>
        <source>Valid options:</source>
        <translation>Gültige Argumenten:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="39"/>
        <source>show this help</source>
        <translation>diese Hilfe anzeigen</translation>
    </message>
    <message>
        <location filename="main.cpp" line="40"/>
        <source>display version</source>
        <translation>version anzeigen</translation>
    </message>
    <message>
        <location filename="main.cpp" line="45"/>
        <source>Unknown option %1</source>
        <translation>Unbekannte Argument %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="46"/>
        <source>Launch %1 -h or %1 --help for help</source>
        <translation>Starten %1 -h oder %1 --help ,um Hilfe zu erhalten</translation>
    </message>
    <message>
        <location filename="main.cpp" line="50"/>
        <source>Launch this application without any parameters to see its main functional</source>
        <translation>Starten die App ohne Argumente, um die Hauptfunktion zu sehen</translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Already Running</source>
        <translation>Schon eingelauft</translation>
    </message>
    <message>
        <location filename="main.cpp" line="85"/>
        <source>Application Already Running</source>
        <translation>Die App ist schon eingelauft</translation>
    </message>
</context>
</TS>
