<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>KLGameField</name>
    <message>
        <location filename="KLGameField.cpp" line="319"/>
        <location filename="KLGameField.cpp" line="394"/>
        <source>This application (*.kgol)</source>
        <oldsource>This application (*.kgol) (*.kgol)</oldsource>
        <translation>Это приложение (*.kgol)</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="387"/>
        <location filename="KLGameField.cpp" line="427"/>
        <source>Error</source>
        <oldsource>error</oldsource>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="332"/>
        <location filename="KLGameField.cpp" line="408"/>
        <source>Open file failed</source>
        <translation>Не удалось открыть файл</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="338"/>
        <location filename="KLGameField.cpp" line="353"/>
        <location filename="KLGameField.cpp" line="363"/>
        <location filename="KLGameField.cpp" line="375"/>
        <source>Invalid file format</source>
        <translation>Неверный формат файла</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="393"/>
        <source>Save colony current state</source>
        <translation>Сохранить текущее состояние колонии</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="434"/>
        <source>Choose cells color</source>
        <translation>Выберите цвет клеток</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="447"/>
        <source>Choose background color</source>
        <translation>Выберите цвет фона</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="318"/>
        <source>Load colony from file</source>
        <translation>Загрузить колонию из файла</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="194"/>
        <source>Game Of Life</source>
        <translation>Игра &quot;Жизнь&quot;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="85"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="214"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="89"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="215"/>
        <source>Colors...</source>
        <oldsource>Colors</oldsource>
        <translation>Цвета...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="216"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="196"/>
        <source>Next Step</source>
        <translation>Следующий шаг</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="169"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="197"/>
        <source>Start/Stop Evolution</source>
        <translation>Начать/Остановить эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="199"/>
        <source>Start Evolution</source>
        <translation>Начать эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="177"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="201"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="202"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="191"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="203"/>
        <source>Save...</source>
        <translation>Сохранить...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="203"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="207"/>
        <source>Open...</source>
        <translation>Загрузить...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="211"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="211"/>
        <source>Cells</source>
        <translation>Клетки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="212"/>
        <source>Background</source>
        <translation>Фон</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="194"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="205"/>
        <source>Save colony current state</source>
        <translation>Сохранить текущее состояние колонии</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="209"/>
        <source>Load colony from file</source>
        <translation>Загрузить колонию из файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="85"/>
        <source>Start evolution</source>
        <translation>Начать эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="213"/>
        <source>Game</source>
        <translation>Игра</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="195"/>
        <source>New Game</source>
        <translation>Новая игра</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="29"/>
        <source>Generation Change Speed</source>
        <translation>Скорость смены поколений</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="39"/>
        <source>Set or erase a single cell by double click or drag a line with left button pressed</source>
        <translation>Установите или снимите клетку двойным нажатием или проведите линию, зажав левую кнопку</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="57"/>
        <source>A simple Game Of Life Qt realization</source>
        <translation>Простая реализация игры &quot;Жизнь&quot; на Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="85"/>
        <source>Stop evolution</source>
        <translation>Остановить эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="89"/>
        <source>Generation: %1</source>
        <translation>Поколение: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="94"/>
        <source>Colony is empty</source>
        <oldsource>Colony is empty or remains static</oldsource>
        <translation>Колония пуста</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="31"/>
        <source>Simple Game Of Life realization</source>
        <oldsource>Simple Game Of Life realization for KDE</oldsource>
        <translation>Простая реализация игры &quot;Жизнь&quot;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="32"/>
        <source>Created by: </source>
        <translation>Создано: </translation>
    </message>
    <message>
        <location filename="main.cpp" line="36"/>
        <source>Usage %1 [options]</source>
        <translation>Использование %1 [параметры]</translation>
    </message>
    <message>
        <location filename="main.cpp" line="38"/>
        <source>Valid options:</source>
        <translation>Допустимые переметры:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="39"/>
        <source>show this help</source>
        <translation>показать эту инструкцию</translation>
    </message>
    <message>
        <location filename="main.cpp" line="40"/>
        <source>display version</source>
        <translation>показать версию</translation>
    </message>
    <message>
        <location filename="main.cpp" line="45"/>
        <source>Unknown option %1</source>
        <translation>Неизвестный параметр %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="46"/>
        <source>Launch %1 -h or %1 --help for help</source>
        <translation>Запустите %1 -h or %1 --help для получения инструкции</translation>
    </message>
    <message>
        <location filename="main.cpp" line="50"/>
        <source>Launch this application without any parameters to see its main functional</source>
        <translation>Запустите без параметров, чтобы увидеть полный функционал</translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Already Running</source>
        <translation>Уже запущено</translation>
    </message>
    <message>
        <location filename="main.cpp" line="85"/>
        <source>Application Already Running</source>
        <oldsource>Linion Already Running</oldsource>
        <translation>Приложение уже запущено</translation>
    </message>
</context>
</TS>
