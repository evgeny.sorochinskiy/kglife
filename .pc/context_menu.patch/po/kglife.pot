# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kglife package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: kglife\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-01 12:14+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:232
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:232
#, kde-format
msgid "GeneralPage"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:233
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:233
#, kde-format
msgid "Borders And Cells"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:234
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:234
#, kde-format
msgid "Background Color"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:235
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:235
#, kde-format
msgid "Cell Color"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:236
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:236
#, kde-format
msgid "Cells Border Color"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:237
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:237
#, kde-format
msgid "Selection Edge Border Color"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:238
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:238
#, kde-format
msgid "Selected Cells Color"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:239
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:239
#, kde-format
msgid "Field Edges Type"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:241
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:241
#, kde-format
msgid "Field is toroidal"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:244
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:244
#, kde-format
msgid ""
"The field is considered to be a toroid. The cell on the edge has its "
"neighbours at the opposite side"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:246
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:246
#, kde-format
msgid "Toroidal"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:248
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:248
#, kde-format
msgid "Alt+S"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:251
#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:256
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:251
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:256
#, kde-format
msgid "Neutral Wall"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:254
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:254
#, kde-format
msgid ""
"The field is surrounded by a neutral wall. No cells are born there and it is "
"not taken into the account as a neighbour"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:258
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:258
#, kde-format
msgid "Alt+W"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:261
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:261
#, kde-format
msgid "Field is infinite (EXPERIMENTAL)"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:267
#, kde-format
msgid ""
"Field is infinite. Cells beyond the edges simply disappear continuing their "
"live cycle. <b>Warning: this is still experimental</b>"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:269
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:269
#, kde-format
msgid "Infinite"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:270
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:270
#, kde-format
msgid "Timer slider"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generalpage.h:271
#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:271
#, kde-format
msgid "Initial Value"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:241
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:241
#, kde-format
msgid "GeneratorPage"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:242
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:242
#, kde-format
msgid "Generator parameters"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:243
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:243
#, kde-format
msgid "Percentage Fill"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:244
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:244
#, kde-format
msgid "Init field before generating"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:245
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:245
#, kde-format
msgid "Generator distribution for X-axis"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:246
#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:250
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:246
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:250
#, kde-format
msgid "Distribution type"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:247
#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:251
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:247
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:251
#, kde-format
msgid "Binomial trial success probability"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:248
#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:252
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:248
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:252
#, kde-format
msgid "Poisson mean value"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_generatorpage.h:249
#: cmake-build-release/kglife_autogen/include/ui_generatorpage.h:249
#, kde-format
msgid "Generator distribution for Y-axis"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_patternspage.h:151
#: cmake-build-release/kglife_autogen/include/ui_patternspage.h:151
#, kde-format
msgid "PatternsPage"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_patternspage.h:152
#: cmake-build-release/kglife_autogen/include/ui_patternspage.h:152
#, kde-format
msgid "Pattern Details"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_patternspage.h:153
#: cmake-build-release/kglife_autogen/include/ui_patternspage.h:153
#, kde-format
msgid "Description:"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_patternspage.h:154
#: cmake-build-debug/kglife_autogen/include/ui_patternspage.h:156
#: cmake-build-debug/kglife_autogen/include/ui_patternspage.h:158
#: cmake-build-release/kglife_autogen/include/ui_patternspage.h:154
#: cmake-build-release/kglife_autogen/include/ui_patternspage.h:156
#: cmake-build-release/kglife_autogen/include/ui_patternspage.h:158
#, kde-format
msgid "TextLabel"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_patternspage.h:155
#: cmake-build-release/kglife_autogen/include/ui_patternspage.h:155
#, kde-format
msgid "Author:"
msgstr ""

#: cmake-build-debug/kglife_autogen/include/ui_patternspage.h:157
#: cmake-build-release/kglife_autogen/include/ui_patternspage.h:157
#, kde-format
msgid "Author E-Mail:"
msgstr ""

#: cmake-build-release/kglife_autogen/include/ui_generalpage.h:267
#, kde-format
msgid ""
"Field is infinite. Cells beyound the edges simply disappear continuing their "
"live cycle. <b>Warning: this is still experimental</b>"
msgstr ""

#: generatorpage.cpp:15
#, kde-format
msgid "Uniform"
msgstr ""

#: generatorpage.cpp:16
#, kde-format
msgid "Binomial"
msgstr ""

#: generatorpage.cpp:17
#, kde-format
msgid "Poisson"
msgstr ""

#. i18n: ectx: Menu (game)
#: kglifeui.rc:11
#, kde-format
msgid "&Game"
msgstr ""

#. i18n: ectx: Menu (selection)
#: kglifeui.rc:19
#, kde-format
msgid "Field"
msgstr ""

#. i18n: ectx: ToolBar (mainToolBar)
#: kglifeui.rc:28
#, kde-format
msgid "Main Toolbar"
msgstr ""

#: KLGameField.cpp:502
#, kde-format
msgid ""
"This application (*.kgol);;Life32/XLife Run Length Encoding (*.rle);;"
"Plaintext (*.cells);;All files(*.*)"
msgstr ""

#: KLGameField.cpp:506
#, kde-format
msgid "Load colony from file"
msgstr ""

#: KLGameField.cpp:519
#, kde-format
msgid ""
"This application (*.kgol);;Life32/XLife Run Length Encoding (*.rle);;"
"Plaintext (*.cells)"
msgstr ""

#: KLGameField.cpp:523
#, kde-format
msgid "Save colony current state"
msgstr ""

#: KLGameField.cpp:579 KLGameField.cpp:595
#, kde-format
msgid ""
"Set or erase a single cell by double click or drag a line with left button "
"pressed"
msgstr ""

#: KLGameField.cpp:580
#, kde-format
msgid "Drag the mouse to move field"
msgstr ""

#: KLGameField.cpp:582 KLGameField.cpp:598
#, kde-format
msgid ""
"Put any cells here by mouse click or simply drag a line of cells.<br>You can "
"also zoom in and out with a mouse wheel or action controls"
msgstr ""

#: KLGameField.cpp:584
#, kde-format
msgid ""
"In move mode you simply drag the mouse to move the field and see the cells "
"beyond the edges"
msgstr ""

#: KLGameField.cpp:596
#, kde-format
msgid "Drag the mouse to select the live cells"
msgstr ""

#: KLGameField.cpp:600
#, kde-format
msgid "In select mode you simply drag the mouse to select the live cells"
msgstr ""

#: KLGameField.cpp:691
#, kde-format
msgid "General"
msgstr ""

#: KLGameField.cpp:692 KLGameField.cpp:693
#, kde-format
msgid "Patterns"
msgstr ""

#: KLGameField.cpp:694 KLGameField.cpp:695
#, kde-format
msgid "Generator"
msgstr ""

#: KLGameField.cpp:722 KLGameField.cpp:826 KLGameField.cpp:877
#: KLGameField.cpp:1027 KLGameField.cpp:1081 KLGameField.cpp:1124
#, kde-format
msgid "Open file failed"
msgstr ""

#: KLGameField.cpp:817 KLGameField.cpp:869 KLGameField.cpp:934
#: KLGameField.cpp:1054 KLGameField.cpp:1097 KLGameField.cpp:1151
#, kde-format
msgid "Error"
msgstr ""

#: KLGameField.cpp:884 KLGameField.cpp:897 KLGameField.cpp:907
#: KLGameField.cpp:920
#, kde-format
msgid "Invalid file format"
msgstr ""

#: KLGameField.cpp:987 mainwindow.cpp:97
#, kde-format
msgid "Print Pattern"
msgstr ""

#: KLGameField.cpp:1045 KLGameField.cpp:1073 KLGameField.cpp:1116
#: mainwindow.cpp:74
#, kde-format
msgid "Colony is empty"
msgstr ""

#: KLGameField.cpp:1249
#, kde-format
msgid "Paste at this position"
msgstr ""

#: KLGameField.cpp:1254
#, kde-format
msgid "Empty"
msgstr ""

#: KLGameField.cpp:1260
#, kde-format
msgid "Fill"
msgstr ""

#: KLGameField.cpp:1266
#, kde-format
msgid "Select untouched"
msgstr ""

#: main.cpp:25
#, kde-format
msgid "Simple Game Of Life realization"
msgstr ""

#: main.cpp:26
#, kde-format
msgid "Created by: "
msgstr ""

#: main.cpp:30
#, kde-format
msgid "Usage %1 [options]"
msgstr ""

#: main.cpp:31
#, kde-format
msgid "Valid options:"
msgstr ""

#: main.cpp:32
#, kde-format
msgid "show this help"
msgstr ""

#: main.cpp:33
#, kde-format
msgid "display version"
msgstr ""

#: main.cpp:38
#, kde-format
msgid "Unknown option %1"
msgstr ""

#: main.cpp:39
#, kde-format
msgid "Launch %1 -h or %1 --help for help"
msgstr ""

#: main.cpp:42
#, kde-format
msgid ""
"Launch this application without any parameters to see its main functional"
msgstr ""

#: main.cpp:74
#, kde-format
msgid "Application Already Running"
msgstr ""

#: main.cpp:74
#, kde-format
msgid "Already Running"
msgstr ""

#: main.cpp:79
#, kde-format
msgid "Game Of Life"
msgstr ""

#: main.cpp:80
#, kde-format
msgid "Conway's Game Of Life for KDE"
msgstr ""

#: main.cpp:82
#, kde-format
msgid "Construct and experiment with many cell colonies"
msgstr ""

#: main.cpp:86
#, kde-format
msgid "Original author"
msgstr ""

#: mainwindow.cpp:53
#, kde-format
msgid "Start evolution"
msgstr ""

#: mainwindow.cpp:53
#, kde-format
msgid "Stop evolution"
msgstr ""

#: mainwindow.cpp:68
#, kde-format
msgid "Generation: %1"
msgstr ""

#: mainwindow.cpp:100
#, kde-format
msgid "Open configuration dialog"
msgstr ""

#: mainwindow.cpp:102
#, kde-format
msgid "Load saved colony"
msgstr ""

#: mainwindow.cpp:104
#, kde-format
msgid "Save current colony"
msgstr ""

#: mainwindow.cpp:107
#, kde-format
msgid "Save current colony with different name"
msgstr ""

#: mainwindow.cpp:115
#, kde-format
msgid "Time step control"
msgstr ""

#: mainwindow.cpp:118
#, kde-format
msgid "New &Game"
msgstr ""

#: mainwindow.cpp:120
#, kde-format
msgid "Clear the field and stop evolution"
msgstr ""

#: mainwindow.cpp:125
#, kde-format
msgid "Generate &Random"
msgstr ""

#: mainwindow.cpp:126
#, kde-format
msgid "Generates random pattern"
msgstr ""

#: mainwindow.cpp:131
#, kde-format
msgid "&Next Step"
msgstr ""

#: mainwindow.cpp:133
#, kde-format
msgid "Next single evolution step"
msgstr ""

#: mainwindow.cpp:138
#, kde-format
msgid "&Start/Stop Game"
msgstr ""

#: mainwindow.cpp:140
#, kde-format
msgid ""
"Start or stop continuous evolution.<br> The icon changes to match the "
"current mode"
msgstr ""

#: mainwindow.cpp:146
#, kde-format
msgid "&Move"
msgstr ""

#: mainwindow.cpp:150
#, kde-format
msgid ""
"Click here to enter field move mode.<br>In this mode you can move the field "
"to see the cells that don't fit the screen due to current zoom"
msgstr ""

#: mainwindow.cpp:160
#, kde-format
msgid "&Mouse selection"
msgstr ""

#: mainwindow.cpp:164
#, kde-format
msgid ""
"Click here to enter select mode.<br>In this mode you can select alive cells "
"to cut and copy"
msgstr ""

#: mainwindow.cpp:170
#, kde-format
msgid "&Clear selection"
msgstr ""

#: mainwindow.cpp:172
#, kde-format
msgid "Click here to clear selection"
msgstr ""

#: mainwindow.cpp:178
#, kde-format
msgid "Zoom scale in. You can also use mouse wheel.<br> Dimmed on maximum zoom"
msgstr ""

#: mainwindow.cpp:180
#, kde-format
msgid ""
"Zoom scale out. You can also use mouse wheel.<br>  Dimmed on minimal zoom"
msgstr ""

#: mainwindow.cpp:184
#, kde-format
msgid "Restore initial zoom.<br>Dimmed when the zoom is minimal"
msgstr ""

#: mainwindow.cpp:203
#, kde-format
msgid "Empty file"
msgstr ""

#: mainwindow.cpp:208
#, kde-format
msgid "File name: %1"
msgstr ""

#: myslider.cpp:24
#, kde-format
msgid "Generation Change Rate"
msgstr ""

#: myslider.cpp:25
#, kde-format
msgid "You can change generation rate even when evolution is in progress"
msgstr ""

#: patternspage.cpp:39
#, kde-format
msgid "<Not selected>"
msgstr ""
