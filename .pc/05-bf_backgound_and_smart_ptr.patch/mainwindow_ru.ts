<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="configdialog.ui" line="14"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="268"/>
        <source>Kglife - Settings</source>
        <translation>Kglife - Настройки</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="90"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="273"/>
        <source>Colors</source>
        <translation>Цвета</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="99"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="275"/>
        <source>Patterns</source>
        <translation>Шаблоны</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="135"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="278"/>
        <source>Cell Color</source>
        <translation>Цвет клетки</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="142"/>
        <location filename="configdialog.ui" line="162"/>
        <location filename="configdialog.ui" line="182"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="279"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="281"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="283"/>
        <source>Select...</source>
        <translation>Выбор...</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="175"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="282"/>
        <source>Cells Border Color</source>
        <translation>Цвет бордюра клетки</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="217"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="284"/>
        <source>Patterns List</source>
        <translation>Список шаблонов</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="233"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="285"/>
        <source>Pattern Details</source>
        <translation>Сведения о шаблоне</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="257"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="286"/>
        <source>Description:</source>
        <translation>Описание:</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="279"/>
        <location filename="configdialog.ui" line="317"/>
        <location filename="configdialog.ui" line="349"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="287"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="289"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="291"/>
        <source>TextLabel</source>
        <translatorcomment>Текст</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="295"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="288"/>
        <source>Author:</source>
        <translation>Автор:</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="327"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="290"/>
        <source>Author E-Mail:</source>
        <translation>E-Mail автора:</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="155"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_configdialog.h" line="280"/>
        <source>Background Color</source>
        <translation>Цвет фона</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="53"/>
        <source>Choose background color</source>
        <translation>Выбирите цвет фона</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="64"/>
        <source>Choose cells color</source>
        <translation>Выбирите цвет клетки</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="75"/>
        <source>Choose border color</source>
        <translation>Выберите цвет бордюра</translation>
    </message>
</context>
<context>
    <name>KLGameField</name>
    <message>
        <location filename="KLGameField.cpp" line="408"/>
        <location filename="KLGameField.cpp" line="421"/>
        <source>This application (*.kgol)</source>
        <oldsource>This application (*.kgol) (*.kgol)</oldsource>
        <translation>Это приложение (*.kgol)</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="446"/>
        <location filename="KLGameField.cpp" line="590"/>
        <source>Error</source>
        <oldsource>error</oldsource>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="431"/>
        <location filename="KLGameField.cpp" line="538"/>
        <source>Open file failed</source>
        <translation>Не удалось открыть файл</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="612"/>
        <source>Info</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="613"/>
        <source>Do you really want to load colony from the selected template?</source>
        <oldsource>Do you really want to load colony from the selected template</oldsource>
        <translation>Вы действительно хотите загрузить выбранный в настройках шаблон?</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="544"/>
        <location filename="KLGameField.cpp" line="556"/>
        <location filename="KLGameField.cpp" line="566"/>
        <location filename="KLGameField.cpp" line="578"/>
        <source>Invalid file format</source>
        <translation>Неверный формат файла</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="420"/>
        <source>Save colony current state</source>
        <translation>Сохранить текущее состояние колонии</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="455"/>
        <source>Set or erase a single cell by double click or drag a line with left button pressed</source>
        <translation>Установите или снимите клетку двойным нажатием или проведите линию, зажав левую кнопку</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="456"/>
        <source>Drag the mouse to move field</source>
        <translation>Зажмите левую кнопку и передвиньте поле с помощью мыши</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="407"/>
        <source>Load colony from file</source>
        <translation>Загрузить колонию из файла</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="273"/>
        <source>Game Of Life</source>
        <translation>Игра &quot;Жизнь&quot;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="90"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="331"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="99"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="332"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="159"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="276"/>
        <source>Clear the field and stop evolution</source>
        <translation>Очистить поле и остановить эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="168"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="278"/>
        <source>Next Step</source>
        <translation>Следующий шаг</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="171"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="280"/>
        <source>Next single evolution step</source>
        <translation>Следующий шаг эволюции</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="180"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="282"/>
        <source>Start/Stop Evolution</source>
        <translation>Начать/Остановить эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="183"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="284"/>
        <source>Start Evolution</source>
        <translation>Начать эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="287"/>
        <source>Start or stop continuous evolution.&lt;br&gt; The  icon changes  to match the current mode</source>
        <oldsource>Start or stop continious evolution.&lt;br&gt; The  icon changes  to match the current mode</oldsource>
        <translation type="unfinished">Начать или остановить эволюцию.&lt;br&gt; Иконка меняется в зависимости от режима</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="289"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="204"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="290"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="291"/>
        <source>Save...</source>
        <translation>Сохранить...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="219"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="296"/>
        <source>Save current colony to file</source>
        <translation>Сохранить колонию в файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="228"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="298"/>
        <source>Open...</source>
        <translation>Загрузить...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="234"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="303"/>
        <source>Load a colony from a file</source>
        <translation>Загрузить колонию из файла</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="252"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="307"/>
        <source>Click here to enter field move mode. In this mode you can move the field to see the cells that don&apos;t fit the screen due to current zoom</source>
        <translation>Нажмите, чтобы перейти в режим перемещения. Перемещайте поле, если не все клетки помещаются на экране из-за масштаба</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="264"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="311"/>
        <source>Zoom scale in. You can also use mouse wheel. &lt;br&gt; Dimmed on maximum zoom</source>
        <translation>Увеличить масштаб. Можно использовать колесо мыши. Затемнена, если масштаб максимальный</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="279"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="315"/>
        <source>Zoom scale out. You can also use mouse wheel.&lt;br&gt;  Dimmed on minimal zoom</source>
        <translation>Уменьшить масштаб. Можно использовать колесо мыши. Затемнена, если масштаб минимальный</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="294"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="319"/>
        <source>Restore initial zoom.&lt;br&gt; Dimmed when the zoom is minimal</source>
        <translation>Восстановить начальный масштаб. Затемнена, если масштаб минимальный</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="303"/>
        <location filename="mainwindow.ui" line="306"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="321"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="323"/>
        <source>Kglife settings...</source>
        <oldsource>Setup Kglife</oldsource>
        <translation>Настроить Kglife...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="309"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="326"/>
        <source>Open settings dialog</source>
        <translation>Показать диалог настроек</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="317"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="328"/>
        <source>Show toolbar</source>
        <translation>Показать панель инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="325"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="329"/>
        <source>Show status bar</source>
        <translation>Показать строку состояния</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="249"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="305"/>
        <source>Move</source>
        <translation>Переместить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="309"/>
        <source>Zoom In</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="276"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="313"/>
        <source>Zoom Out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="291"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="317"/>
        <source>Restore</source>
        <translation>Восстановить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="293"/>
        <source>Save colony current state</source>
        <translation>Сохранить текущее состояние колонии</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="231"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="300"/>
        <source>Load colony from file</source>
        <translation>Загрузить колонию из файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="112"/>
        <source>Start evolution</source>
        <translation>Начать эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="330"/>
        <source>Game</source>
        <translation>Игра</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="156"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="274"/>
        <source>New Game</source>
        <translation>Новая игра</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="37"/>
        <source>Generation Change Rate</source>
        <oldsource>Generation Change Speed</oldsource>
        <translation>Скорость смены поколений</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="39"/>
        <source>You can change generation rate even when evolution is in progress</source>
        <translation>Можно изменять скорость смены поколений даже во время эволюции</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="50"/>
        <source>Put any cells here by mouse click or simply drag a line of cells.&lt;br&gt;You can also zoom in and out whith a mouse wheel or action controls</source>
        <oldsource>Put any cells here by mouse click or simply drag a line of cells.&lt;br&gt;You can also increase or decrease the scale whith a mouse wheel or menu controls</oldsource>
        <translation>Расположите любое количество клеток щелчком мыши или проведите линию, зажав левую кнопку. Можно изменять масштаб при помощи колеса мыши или элементов управления</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="70"/>
        <source>A simple Game Of Life Qt realization</source>
        <translation>Простая реализация игры &quot;Жизнь&quot; на Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="112"/>
        <source>Stop evolution</source>
        <translation>Остановить эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="116"/>
        <source>Generation: %1</source>
        <translation>Поколение: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="121"/>
        <source>Colony is empty</source>
        <oldsource>Colony is empty or remains static</oldsource>
        <translation>Колония пуста</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="31"/>
        <source>Simple Game Of Life realization</source>
        <oldsource>Simple Game Of Life realization for KDE</oldsource>
        <translation>Простая реализация игры &quot;Жизнь&quot;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="32"/>
        <source>Created by: </source>
        <translation>Создано: </translation>
    </message>
    <message>
        <location filename="main.cpp" line="36"/>
        <source>Usage %1 [options]</source>
        <translation>Использование %1 [параметры]</translation>
    </message>
    <message>
        <location filename="main.cpp" line="38"/>
        <source>Valid options:</source>
        <translation>Допустимые переметры:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="39"/>
        <source>show this help</source>
        <translation>показать эту инструкцию</translation>
    </message>
    <message>
        <location filename="main.cpp" line="40"/>
        <source>display version</source>
        <translation>показать версию</translation>
    </message>
    <message>
        <location filename="main.cpp" line="45"/>
        <source>Unknown option %1</source>
        <translation>Неизвестный параметр %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="46"/>
        <source>Launch %1 -h or %1 --help for help</source>
        <translation>Запустите %1 -h or %1 --help для получения инструкции</translation>
    </message>
    <message>
        <location filename="main.cpp" line="50"/>
        <source>Launch this application without any parameters to see its main functional</source>
        <translation>Запустите без параметров, чтобы увидеть полный функционал</translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Already Running</source>
        <translation>Уже запущено</translation>
    </message>
    <message>
        <location filename="main.cpp" line="85"/>
        <source>Application Already Running</source>
        <oldsource>Linion Already Running</oldsource>
        <translation>Приложение уже запущено</translation>
    </message>
</context>
</TS>
