//
// Created by esorochinskiy on 27.11.22.
//

#ifndef KGLIFE_PREFERENCES_H
#define KGLIFE_PREFERENCES_H

#include <QString>
#include <QVariant>

typedef QHash<QString, QVariant> PreferencesType;
#endif //KGLIFE_PREFERENCES_H
