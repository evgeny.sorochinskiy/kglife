<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="configdialog.ui" line="14"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="268"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="268"/>
        <source>Kglife - Settings</source>
        <translation>Kglife - Setzung</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="90"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="273"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="273"/>
        <source>Colors</source>
        <translation>Farben</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="99"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="275"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="275"/>
        <source>Patterns</source>
        <translation>Modelle</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="135"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="278"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="278"/>
        <source>Cell Color</source>
        <translation>Einzellefarbe</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="142"/>
        <location filename="configdialog.ui" line="162"/>
        <location filename="configdialog.ui" line="182"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="279"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="281"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="283"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="279"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="281"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="283"/>
        <source>Select...</source>
        <translation>Anwahl...</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="175"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="282"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="282"/>
        <source>Cells Border Color</source>
        <translation>Einzellerandfarbe</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="217"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="284"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="284"/>
        <source>Patterns List</source>
        <translation>Modelleliste</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="233"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="285"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="285"/>
        <source>Pattern Details</source>
        <translation>Einzelheiten zum Model</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="257"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="286"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="286"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="279"/>
        <location filename="configdialog.ui" line="317"/>
        <location filename="configdialog.ui" line="349"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="287"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="289"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="291"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="287"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="289"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="291"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="295"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="288"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="288"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="327"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="290"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="290"/>
        <source>Author E-Mail:</source>
        <translation>EMail von Autor:</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="155"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_configdialog.h" line="280"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_configdialog.h" line="280"/>
        <source>Background Color</source>
        <translation>Hintergrundfarbe</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="63"/>
        <source>Choose background color</source>
        <translation>Hintergrundfarbe anwahlen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="74"/>
        <source>Choose cells color</source>
        <translation>Einzellefarbe anwahlen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="85"/>
        <source>Choose border color</source>
        <translation>Einzellerandfarbe anwahlen</translation>
    </message>
</context>
<context>
    <name>KLGameField</name>
    <message>
        <location filename="KLGameField.cpp" line="408"/>
        <source>Load colony from file</source>
        <translation>Kolonie von der Datei einladen</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="409"/>
        <location filename="KLGameField.cpp" line="422"/>
        <source>This application (*.kgol)</source>
        <translation>Diese App (*.kgol)</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="432"/>
        <location filename="KLGameField.cpp" line="537"/>
        <source>Open file failed</source>
        <translation>Dateiladen Versager</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="613"/>
        <source>Info</source>
        <translation>Vorsicht</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="614"/>
        <source>Do you really want to load colony from the selected template?</source>
        <oldsource>Do you really want to load colony from the selected template</oldsource>
        <translation>Wollen Sie wirklich eine ausgewählte Kolonie anladen?</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="543"/>
        <location filename="KLGameField.cpp" line="555"/>
        <location filename="KLGameField.cpp" line="565"/>
        <location filename="KLGameField.cpp" line="577"/>
        <source>Invalid file format</source>
        <translation>Ungültiges Dateiformat</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="447"/>
        <location filename="KLGameField.cpp" line="589"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="421"/>
        <source>Save colony current state</source>
        <translation>Den Koloniezustand sparen</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="456"/>
        <source>Set or erase a single cell by double click or drag a line with left button pressed</source>
        <translation>Mausdoppelklick um die Einzelle setzen oder löschen</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="457"/>
        <source>Drag the mouse to move field</source>
        <translation>Linke Maustaste halten um das Feld zu bewegen</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="273"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="273"/>
        <source>Game Of Life</source>
        <translation>Lebenspiel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="330"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="330"/>
        <source>Game</source>
        <translation>Spiel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="90"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="331"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="331"/>
        <source>Settings</source>
        <translation>Setzen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="99"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="332"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="332"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="156"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="274"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="274"/>
        <source>New Game</source>
        <translation>Neues Spiel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="159"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="276"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="276"/>
        <source>Clear the field and stop evolution</source>
        <translation>Aufklaren das Spielfeld und die Evolution stoppen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="168"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="278"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="278"/>
        <source>Next Step</source>
        <translation>Nächster Schritt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="171"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="280"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="280"/>
        <source>Next single evolution step</source>
        <translation>Nächster Evolutionsschritt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="180"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="282"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="282"/>
        <source>Start/Stop Evolution</source>
        <translation>Start/Stop die Evolution</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="183"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="284"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="284"/>
        <source>Start Evolution</source>
        <translation>Start die Evolution</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="287"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="287"/>
        <source>Start or stop continuous evolution.&lt;br&gt; The  icon changes  to match the current mode</source>
        <oldsource>Start or stop continious evolution.&lt;br&gt; The  icon changes  to match the current mode</oldsource>
        <translation type="unfinished">Kontinuierliche Weiterentwicklung starten oder stoppen.&lt;br&gt; Das Symbol ändert sich entsprechend dem aktuellen Modus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="289"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="289"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="204"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="290"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="290"/>
        <source>About</source>
        <translation>Über diese App</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="291"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="291"/>
        <source>Save...</source>
        <translation>Sparen...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="293"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="293"/>
        <source>Save colony current state</source>
        <translation>Den Koloniezustand sparen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="219"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="296"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="296"/>
        <source>Save current colony to file</source>
        <translation>Den Koloniezustand zur Datei sparen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="228"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="298"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="298"/>
        <source>Open...</source>
        <translation>Laden...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="231"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="300"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="300"/>
        <source>Load colony from file</source>
        <translation>Den Koloniezustand einladen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="234"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="303"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="303"/>
        <source>Load a colony from a file</source>
        <translation>Den Koloniezustand von Datei einladen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="252"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="307"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="307"/>
        <source>Click here to enter field move mode. In this mode you can move the field to see the cells that don&apos;t fit the screen due to current zoom</source>
        <translation>Machen Sie eine Mausclick, um das Feldverschiebungsmodus zu eintragen. Mit diesem Modus können Sie das Feld verschieben, um die Zellen, die den Bildschirm nicht passen wegen вуы Maßstabs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="264"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="311"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="311"/>
        <source>Zoom scale in. You can also use mouse wheel. &lt;br&gt; Dimmed on maximum zoom</source>
        <translation>Zunehmen den Maßstab. Können Sie auch das Mausrad benutzen &lt;br&gt; Dunkelt when der Maßstab maximum ist</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="279"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="315"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="315"/>
        <source>Zoom scale out. You can also use mouse wheel.&lt;br&gt;  Dimmed on minimal zoom</source>
        <translation>Verringern den Maßstab. Können Sie auch das Mausrad benutzen &lt;br&gt; Dunkelt when der Maßstab minimum ist</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="294"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="319"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="319"/>
        <source>Restore initial zoom.&lt;br&gt; Dimmed when the zoom is minimal</source>
        <translation>Wiederherstellen den Maßstab.&lt;br&gt; Dunkelt when der Maßstab minimum ist</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="303"/>
        <location filename="mainwindow.ui" line="306"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="321"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="323"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="321"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="323"/>
        <source>Kglife settings...</source>
        <oldsource>Setup Kglife</oldsource>
        <translation>Kglife besetzen...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="309"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="326"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="326"/>
        <source>Open settings dialog</source>
        <translation>Besetzungefenster aufmachen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="317"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="328"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="328"/>
        <source>Show toolbar</source>
        <translation>Symbolleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="325"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="329"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="329"/>
        <source>Show status bar</source>
        <translation>Statusleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="249"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="305"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="305"/>
        <source>Move</source>
        <translation>Bewegen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="309"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="309"/>
        <source>Zoom In</source>
        <translation>Zunehmen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="276"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="313"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="313"/>
        <source>Zoom Out</source>
        <translation>Verringern</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="291"/>
        <location filename="cmake-build-debug/kglife_autogen/include/ui_mainwindow.h" line="317"/>
        <location filename="cmake-build-release/kglife_autogen/include/ui_mainwindow.h" line="317"/>
        <source>Restore</source>
        <translation>Wiederherstellen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="45"/>
        <source>Generation Change Rate</source>
        <oldsource>Generation Change Speed</oldsource>
        <translation>Veränderengeschwindigkeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="47"/>
        <source>You can change generation rate even when evolution is in progress</source>
        <translation>Veränderen Sie die Geburstgeschwindigkeit sogar wenn die Evolution progreßiert</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="58"/>
        <source>Put any cells here by mouse click or simply drag a line of cells.&lt;br&gt;You can also zoom in and out whith a mouse wheel or action controls</source>
        <oldsource>Put any cells here by mouse click or simply drag a line of cells.&lt;br&gt;You can also increase or decrease the scale whith a mouse wheel or menu controls</oldsource>
        <translation>Stellen Sie hier mit Mausklick beliebige Zellen ein oder ziehen Sie einfach eine Zelllinie auf</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="78"/>
        <source>A simple Game Of Life Qt realization</source>
        <translation>Die einfrache Lebenspielrealisation</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Start evolution</source>
        <translation>Start die Evolution</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Stop evolution</source>
        <translation>Stop die Evolution</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="124"/>
        <source>Generation: %1</source>
        <translation>Generation %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <source>Colony is empty</source>
        <translation>Leerkolonie</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="31"/>
        <source>Simple Game Of Life realization</source>
        <oldsource>Simple Game Of Life realization for KDE</oldsource>
        <translation>Die einfache Lebenspielrealisation</translation>
    </message>
    <message>
        <location filename="main.cpp" line="32"/>
        <source>Created by: </source>
        <translation>Erstellt von: </translation>
    </message>
    <message>
        <location filename="main.cpp" line="36"/>
        <source>Usage %1 [options]</source>
        <translation>Benutzen: %1 [Argumente]</translation>
    </message>
    <message>
        <location filename="main.cpp" line="38"/>
        <source>Valid options:</source>
        <translation>Gültige Argumenten:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="39"/>
        <source>show this help</source>
        <translation>diese Hilfe anzeigen</translation>
    </message>
    <message>
        <location filename="main.cpp" line="40"/>
        <source>display version</source>
        <translation>version anzeigen</translation>
    </message>
    <message>
        <location filename="main.cpp" line="45"/>
        <source>Unknown option %1</source>
        <translation>Unbekannte Argument %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="46"/>
        <source>Launch %1 -h or %1 --help for help</source>
        <translation>Starten %1 -h oder %1 --help ,um Hilfe zu erhalten</translation>
    </message>
    <message>
        <location filename="main.cpp" line="50"/>
        <source>Launch this application without any parameters to see its main functional</source>
        <translation>Starten die App ohne Argumente, um die Hauptfunktion zu sehen</translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Already Running</source>
        <translation>Schon eingelauft</translation>
    </message>
    <message>
        <location filename="main.cpp" line="85"/>
        <source>Application Already Running</source>
        <translation>Die App ist schon eingelauft</translation>
    </message>
</context>
</TS>
