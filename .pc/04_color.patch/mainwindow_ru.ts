<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>KLGameField</name>
    <message>
        <location filename="KLGameField.cpp" line="404"/>
        <location filename="KLGameField.cpp" line="473"/>
        <source>This application (*.kgol)</source>
        <oldsource>This application (*.kgol) (*.kgol)</oldsource>
        <translation>Это приложение (*.kgol)</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="465"/>
        <location filename="KLGameField.cpp" line="498"/>
        <source>Error</source>
        <oldsource>error</oldsource>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="413"/>
        <location filename="KLGameField.cpp" line="483"/>
        <source>Open file failed</source>
        <translation>Не удалось открыть файл</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="419"/>
        <location filename="KLGameField.cpp" line="431"/>
        <location filename="KLGameField.cpp" line="441"/>
        <location filename="KLGameField.cpp" line="453"/>
        <source>Invalid file format</source>
        <translation>Неверный формат файла</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="472"/>
        <source>Save colony current state</source>
        <translation>Сохранить текущее состояние колонии</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="505"/>
        <source>Choose cells color</source>
        <translation>Выберите цвет клеток</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="518"/>
        <source>Choose background color</source>
        <translation>Выберите цвет фона</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="532"/>
        <source>Set or erase a single cell by double click or drag a line with left button pressed</source>
        <translation>Установите или снимите клетку двойным нажатием или проведите линию, зажав левую кнопку</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="533"/>
        <source>Drag the mouse to move field</source>
        <translation>Зажмите левую кнопку и передвиньте поле с помощью мыши</translation>
    </message>
    <message>
        <location filename="KLGameField.cpp" line="403"/>
        <source>Load colony from file</source>
        <translation>Загрузить колонию из файла</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="241"/>
        <source>Game Of Life</source>
        <translation>Игра &quot;Жизнь&quot;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="90"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="265"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="266"/>
        <source>Colors...</source>
        <oldsource>Colors</oldsource>
        <translation>Цвета...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="267"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="170"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="243"/>
        <source>Next Step</source>
        <translation>Следующий шаг</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="179"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="244"/>
        <source>Start/Stop Evolution</source>
        <translation>Начать/Остановить эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="246"/>
        <source>Start Evolution</source>
        <translation>Начать эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="187"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="248"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="192"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="249"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="250"/>
        <source>Save...</source>
        <translation>Сохранить...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="254"/>
        <source>Open...</source>
        <translation>Загрузить...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="221"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="258"/>
        <source>Cells</source>
        <translation>Клетки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="226"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="259"/>
        <source>Background</source>
        <translation>Фон</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="241"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="260"/>
        <source>Move</source>
        <translation>Переместить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="261"/>
        <source>Zoom In</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="262"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="262"/>
        <source>Zoom Out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="273"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="263"/>
        <source>Restore</source>
        <translation>Восстановить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="204"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="252"/>
        <source>Save colony current state</source>
        <translation>Сохранить текущее состояние колонии</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="256"/>
        <source>Load colony from file</source>
        <translation>Загрузить колонию из файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="94"/>
        <source>Start evolution</source>
        <translation>Начать эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="264"/>
        <source>Game</source>
        <translation>Игра</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="161"/>
        <location filename="obj-x86_64-linux-gnu/kglife_autogen/include/ui_mainwindow.h" line="242"/>
        <source>New Game</source>
        <translation>Новая игра</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="29"/>
        <source>Generation Change Speed</source>
        <translation>Скорость смены поколений</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="58"/>
        <source>A simple Game Of Life Qt realization</source>
        <translation>Простая реализация игры &quot;Жизнь&quot; на Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="94"/>
        <source>Stop evolution</source>
        <translation>Остановить эволюцию</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="98"/>
        <source>Generation: %1</source>
        <translation>Поколение: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Colony is empty</source>
        <oldsource>Colony is empty or remains static</oldsource>
        <translation>Колония пуста</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="31"/>
        <source>Simple Game Of Life realization</source>
        <oldsource>Simple Game Of Life realization for KDE</oldsource>
        <translation>Простая реализация игры &quot;Жизнь&quot;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="32"/>
        <source>Created by: </source>
        <translation>Создано: </translation>
    </message>
    <message>
        <location filename="main.cpp" line="36"/>
        <source>Usage %1 [options]</source>
        <translation>Использование %1 [параметры]</translation>
    </message>
    <message>
        <location filename="main.cpp" line="38"/>
        <source>Valid options:</source>
        <translation>Допустимые переметры:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="39"/>
        <source>show this help</source>
        <translation>показать эту инструкцию</translation>
    </message>
    <message>
        <location filename="main.cpp" line="40"/>
        <source>display version</source>
        <translation>показать версию</translation>
    </message>
    <message>
        <location filename="main.cpp" line="45"/>
        <source>Unknown option %1</source>
        <translation>Неизвестный параметр %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="46"/>
        <source>Launch %1 -h or %1 --help for help</source>
        <translation>Запустите %1 -h or %1 --help для получения инструкции</translation>
    </message>
    <message>
        <location filename="main.cpp" line="50"/>
        <source>Launch this application without any parameters to see its main functional</source>
        <translation>Запустите без параметров, чтобы увидеть полный функционал</translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Already Running</source>
        <translation>Уже запущено</translation>
    </message>
    <message>
        <location filename="main.cpp" line="85"/>
        <source>Application Already Running</source>
        <oldsource>Linion Already Running</oldsource>
        <translation>Приложение уже запущено</translation>
    </message>
</context>
</TS>
